import { Component } from "react";

import background from "../../../assets/images/landscape-photography_1645.jpg"

class HeaderImage extends Component {
 render (){
    return (
        <div className="mt-4">
        <img height="300px" width="500px" src={background} alt="ảnh" />
      </div>
    )
 }
}

export default HeaderImage