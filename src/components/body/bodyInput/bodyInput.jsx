import { Component } from "react";

class BodyInput extends Component {
    change(event){
        console.log(event.target.value)
    }
    render() {
        return (
            <div className="row mt-4">
                <label>Message cho bạn 12 tháng tới</label>
                <input placeholder="Nhập message của bạn vào đây" className="form-control mt-3" onChange={this.change}></input>
            </div>
        )
    }
}

export default BodyInput