import { Component } from "react";
import like from '../../../assets/images/tải xuống.png'


class BodyOutput extends Component {
    click (){
        console.log('Gửi thông điệp được ấn')
    }
    render() {
        return (
            <>
                <div className="row mt-3">
                    <button className="btn btn-success m-auto" style={{ width: "150px" }} onClick={this.click}>Gửi thông điệp</button>
                </div>
                <div className="mt-4">
                    <img height="100px" width="100px" src={like} alt="likeImages" />
                </div>
            </>
        )
    }
}
export default BodyOutput