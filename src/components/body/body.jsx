import { Component } from "react";
import BodyInput from "./bodyInput/bodyInput";
import BodyOutput from "./bodyOutput/bodyOutout";

class Body extends Component {
    render() {
        return (
            <>
                <BodyInput />
                <BodyOutput/>
            </>
        )
    }
}

export default Body